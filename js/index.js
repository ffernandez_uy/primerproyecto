$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel({
    inteval:3000
  }); 
  $('#contacto').on('show.bs.moda', function (e){
    console.log('el modal contacto se esta mostrando');

    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disable', true);
  });
  $('#contacto').on('shown.bs.moda', function (e){
    console.log('el modal contacto se mostró');
  });
  $('#contacto').on('hide.bs.moda', function (e){
    console.log('el modal contacto se oculta');
  });
  $('#contacto').on('hidden.bs.moda', function (e){
    console.log('el modal contacto se ocultó');
    $().prop('disable', false);
  });
        
});